# -*- coding: utf-8 -*-
from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from main.models import Share, ShareCost

class ShareCostAdmin(ImportExportModelAdmin):
    list_display = ['share', 'cost']
    list_filter = ['share']

admin.site.register(Share, ImportExportModelAdmin)
admin.site.register(ShareCost, ShareCostAdmin)
