# -*- coding: utf-8 -*-
from unittest import TestCase

from main.markus import income_and_diff
from main.models import Share, ShareCost
from django.utils import timezone

# http://financetoys.com/portfolio/markrus.htm
class CalculateTestCase(TestCase):

    def setUp(self, share=Share(name='APPLE')):
        Share.objects.bulk_create([
            share,
            Share(name='SAMSUNG'),
            Share(name='LG'),
        ])
        self.samsung_share = Share.objects.get(name='SAMSUNG')
        now = timezone.now()
        # Цены для самсунга. оригинал 230,9
        ShareCost.objects.bulk_create([
            ShareCost(share=self.samsung_share, cost=235.97, change=2.2, time=now),
            ShareCost(share=self.samsung_share, cost=241.79, change=2.5, time=now),
            ShareCost(share=self.samsung_share, cost=247.4, change=2.3, time=now),
        ])
        
    def test_income_and_diff(self):
        matrix = income_and_diff(ShareCost.objects.filter(share=self.samsung_share))
        for matrix_item in matrix:
            share_cost_middle, share_diff_value, dd = matrix_item
        self.assertEqual(share_cost_middle, (235.97+241.79+247.4)/3)