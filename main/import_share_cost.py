#! coding=utf-8
from __future__ import unicode_literals, absolute_import
import csv
import urllib
import datetime

import io

from main.models import Share, ShareCost


def to_db_list(dict_obj):
    share = Share.objects.get_or_create(name=dict_obj['<TICKER>'])[0]
    cost = float(dict_obj['<OPEN>'])
    if ShareCost.objects.filter(share=share).exists():
        first_cost = ShareCost.objects.filter(share=share).last().cost
        change = (cost-first_cost)/first_cost*100
    else:
        change = 0
    share_cost_dict = {
            'cost': cost,
            'time': datetime.datetime.strptime(dict_obj['<DATE>'], '%Y%m%d'),
            'change': change,
            'share': share
        }
    if not ShareCost.objects.filter(share=share, time=share_cost_dict['time']).exists():
         ShareCost.objects.create(**share_cost_dict)
    else:
         ShareCost.objects.filter(share=share, time=share_cost_dict['time']).update(**share_cost_dict)
#https://www.finam.ru/profile/moex-akcii/gdr-ros-agro-plc-ord-shs/export/?market=1&em=399716&code=AGRO&apply=0&df=1&mf=8&yf=2000&from=01.09.2000&dt=6&mt=5&yt=2017&to=06.06.2017&p=8&f=AGRO_000901_170606&e=.csv&cn=AGRO&dtf=1&tmf=1&MSOR=1&mstime=on&mstimever=1&sep=3&sep2=1&datf=1&at=1
def load_share_cost():
    code_list = ['AGRO', 'GASP', 'lukoil', 'lkoh', 'sberbank', 'gazprom']
    for code in code_list:

        url = ('http://export.finam.ru/{code}_000901_170606.csv?market=1&em=399716&code={code}&apply=0&df=1&mf=8&yf=2000&from=01.09.2000&dt=6&mt=5&yt=2017&to=06.06.2017&p=8&f={code}_000901_170606&e=.csv&cn={code}'
               '&dtf=1&tmf=1&MSOR=1&mstime=on&mstimever=1&sep=1&sep2=1&datf=1&at=1'.format(code=code))

        response = urllib.request.urlopen(url)
        csvfilebuffer = io.BytesIO(response.read())
        csvfile = open('tmp.txt', 'wb')
        csvfile.write(csvfilebuffer.read())
        csvfile.close()
        with open('tmp.txt', 'r') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
            res = []
            for row in spamreader:
                i = 0
                if spamreader.line_num == 1:
                    mapper = row[0].split(',')

                else:
                    res_item = {}
                    for item in row[0].split(','):
                        res_item[mapper[i]]=item
                        i += 1
                    to_db_list(res_item)




