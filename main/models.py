# -*- coding: utf-8 -*-
from django.db import models
import numpy as np


class Share(models.Model):
    name = models.CharField(verbose_name=u'Уникальное название', max_length=255)

    def __str__(self):
        return self.name

    def get_cost(self):
        cost_list = [cost.cost for cost in self.costs.all()]
        return np.std(cost_list)


class ShareCost(models.Model):

    time = models.DateTimeField(verbose_name=u'Время')
    cost = models.FloatField(verbose_name=u'стоимость')
    change = models.FloatField(verbose_name=u'% изменение')
    share = models.ForeignKey(verbose_name=u'Компания', to=Share, related_name='costs')
    class Meta:
        verbose_name = u'Котировка'
        verbose_name_plural = u'Котировки'

    def __str__(self):
        return self.share.name
