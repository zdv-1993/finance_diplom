# -*- coding: utf-8 -*-
import io

import numpy as np

from django.conf import settings
from matplotlib.backends.backend_agg import FigureCanvas
from matplotlib.figure import Figure

from main.models import ShareCost


def get_path(filename):
    return '{}/graph/{}'.format(settings.MEDIA_ROOT, filename)

def get_url(filename):
    return '{}graph/{}'.format(settings.MEDIA_URL, filename)

# Получаем формулу прямой
def get_y_from_x_equation(x, x1, y1, x2, y2):
    return (x-x1)*(y2-y1)/(x2-x1)+y1

def get_trigonal_function(x, a, b, c):
    if x <= a:
        return 0
    elif a <= x <= b:
        return (x-a)/(b-a)
    elif c <=x:
        return 0

def run(income, qs=ShareCost.objects.all()):
    import matplotlib.pyplot as plt
    from matplotlib import pylab
    fig = Figure()
    ax = fig.add_subplot(111)

    # share_list = set([(item.share.id, item.share.name) for item in qs])
    company_list = []
    # Вычисляем r среднее
    # ожидаемая доходность акции. Равна среднему доходу за выбраный период
    share_cost_list = [item.cost for item in qs]
    share_cost_mean_value = np.mean(share_cost_list)

    # стандартное отклонение акции, которое является измерителем показателя риска
    share_diff_list = [item.change for item in qs]
    share_cost_list = [item.cost for item in qs]
    share_diff_value = np.std(share_diff_list)
    share_min_value = np.min(share_diff_list)
    share_max_value = np.max(share_diff_list)

    cost_min = np.min(share_cost_list)
    cost_max = np.max(share_cost_list)
    cost_diff = np.std(share_cost_list)

    company_list.append({
        # 'name': share_item_name,
        'diff_min': share_min_value,
        'diff_max': share_max_value,
        'diff': share_diff_value,
        'cost_min': cost_min,
        'cost_max': cost_max,
        'cost_diff': cost_diff,
    })
    # fig = plt.figure()
    # import pdb; pdb.set_trace()
    for company in company_list:

        a=company['diff_min']
        b=company['diff']
        c=company['diff_max']

        x = np.arange(a, b, 0.1)
        r1 = 1 - (b-x)/(b-a)
        ax.plot(x, r1)
        ax.plot(x, r1)
        x = np.arange(b, c, 0.1)
        r2 = 1 - (x-b)/(c-b)
        ax.plot(x, r2)

        local_diff = (company['diff_max']- company['diff_min'])/2
        a = local_diff - local_diff*income
        b = local_diff
        c = local_diff + local_diff*income
        x = np.arange(a, b, 0.001)
        r1 = 1 - (b - x) / (b - a)
        ax.plot(x, r1)
        x = np.arange(b, c, 0.001)
        r2 = 1 - (x - b) / (c - b)
        ax.plot(x, r2)

        # x = np.arange(b, c)
        # r3 = 0*x
        # ax.plot(x, r3)
        # win = fig.canvas.manager.window
        # win.after(100, run)

    # plt.savefig(get_path('graph.png'))
    # plt.close()
    canvas = FigureCanvas(fig)
    # buf = io.BytesIO()
    canvas.print_png(get_path('graph.png'))
