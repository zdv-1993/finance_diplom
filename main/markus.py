# -*- coding: utf-8 -*-

import numpy as np


#  Найти ожидаемый доход и стандартное отклонение для каждой акции
from main.models import ShareCost


def income_and_diff(portfolio):
    """
    :param portfolio
    :return: matrix
    """
    matrix = []
    # получаем матрицу
    for portfolio_item in portfolio['list']:

        # ожидаемая доходность акции. Равна среднему доходу за выбраный период
        share_cost_list = [item.cost for item in ShareCost.objects.filter(share=portfolio_item['share'])]
        share_cost_item = ShareCost.objects.filter(share=portfolio_item['share']).first()
        share_cost_mean_value = np.mean(share_cost_list)
        share_cost_original = share_cost_item.cost*(100+share_cost_item.change)/100
        share_cost_income = ((share_cost_mean_value-share_cost_original)/share_cost_original)*100
        # стандартное отклонение акции, которое является измерителем показателя риска
        share_diff_list = [item.change for item in ShareCost.objects.filter(share=portfolio_item['share'])]
        share_diff_value = np.mean(share_diff_list)

        matrix.append([share_cost_income, share_diff_value, (portfolio_item['count']/portfolio['count'])*100])
    return matrix


def markus_run(expected_profit, portfolio=None):
    # ШАГ 2: Найти ожидаемый доход и стандартное отклонение для каждой акции

    matrix = income_and_diff(portfolio)
    np_matrix = np.array(matrix)
    step_2 = np_matrix.copy()
    # вычисляем количество компаний
    company_count = np_matrix.shape[0]
    # ШАГ 3. Рисуем два единичных вектора
    u = np.array([[1 for i in range(company_count)]])
    uT = u.T

    e = np.array([[i] for i in np_matrix[:, 0]])
    d = np_matrix[:, 1]
    w = np_matrix[:, 2]
    w_old = w
    # ШАГ 4. Рисуем две транспонированные матрицы для ожидаемых доходов и весов

    wT = w.T
    dT = d.T
    eT = e.T

    # ШАГ 5. Создаем ковариационную матрицу

    V = np.cov(np_matrix)
    step_5 = V.copy()
    # ШАГ 6. Определяем риск (стандартное отклонение) портфеля
    standart_diff = (wT * V) * w

    step_6 = standart_diff.copy()

    #ШАГ 7. Найдем обратную матрицу

    V_invert = np.linalg.inv(V)
    # ШАГ 8. Определим 4 скалярных величины
    A = np.linalg.det(uT * V_invert * e)

    B = np.linalg.det(eT * V_invert)
    C = np.linalg.det(uT * V_invert * u)
    D = B * C - A * A

    # step_7_A = A.copy()
    # step_7_B = B.copy()
    # step_7_C = C.copy()
    # step_7_D = D.copy()
    #Шаг 9.Расчет промежуточных коэффициентов m и l
    m = V_invert * u
    l = V_invert * e
    # Шаг 10. Рассчет координат портфеля
    # g и h являются двумя точками на эффективной границе
    g = (B * m - A * l)/D
    h = (C * l - A * m)/D
    # ШАГ 11. Находим эффективный портфель для заданной доходности

    # Доходность портфеля
    T = np.array([expected_profit for i in range(np_matrix.shape[0])])

    w_matrix = g * 100 + (h * 100) * T
    w_vector = np.array([np.mean(w_matrix[i]) for i in range(w_matrix.shape[0])])
    wT = w.T
    # Сделайте ссылки из столбца g + h x T = w в столбец w (веса) в таюлице 1, ШАГ 2
    risk = np.linalg.det(V * w * wT)
    np_matrix[:, 2] = w_vector
    import pudb;
    pudb.set_trace()
    return step_2, step_5, step_6
