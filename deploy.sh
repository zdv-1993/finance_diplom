#!/bin/bash
ssh -tt root@tyagach34.ru << EOF
    su www-data
    cd /var/www/finance/src
    git pull origin master
    source ../env/bin/activate
    pip install -r requirements.txt
    python manage.py collectstatic --noinput
    python manage.py migrate --noinput
    supervisorctl reload finance
EOF

