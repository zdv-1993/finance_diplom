# -*- coding: utf-8 -*-
from django import forms

from main.models import Share


class PortfolioForm(forms.Form):
    company_action = forms.ModelChoiceField(queryset=Share.objects.all())
    count = forms.IntegerField()