# -*- coding: utf-8 -*-
from django.http import HttpResponse
from django.views.generic import TemplateView
from django.forms import formset_factory

from finance.forms import PortfolioForm
from main import calculate
from main.calculate import get_url
from main.markus import markus_run
from main.models import Share


from multiprocessing.dummy import Pool as ThreadPool

pool = ThreadPool(4)

PortfolioFormset = formset_factory(PortfolioForm)

class IndexView(TemplateView):
    template_name = 'index.html'
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['share_list'] = Share.objects.all()
        context['portfolio_formset'] = PortfolioFormset(initial=[
         {'company_action': share,
          'count': 1,} for share in Share.objects.all()
        ])
        # if self.request.POST and self.request.POST.get('number'):

        return context
    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        calculate.run(income=float(self.request.POST['income']))
        formset = PortfolioFormset(self.request.POST)
        if formset.is_valid():
            portfolio = {}
            portfolio['list'] = []
            share_items_id = []
            summ = 0
            count = 0
            for form in formset:
                if form.cleaned_data.get('company_action'):
                    share = form.cleaned_data['company_action']
                    share_items_id.append(share.id)
                    portfolio_item_count = form.cleaned_data['count']
                    portfolio_item_summ = share.get_cost() * portfolio_item_count
                    summ += portfolio_item_summ
                    count += int(portfolio_item_count)
                    portfolio['list'].append({
                        'mean_cost': share.get_cost(),
                        'name': share.name,
                        'count': portfolio_item_count,
                        'summ_items': portfolio_item_summ,
                        'share': share
                    })
            portfolio['summ'] = summ
            portfolio['count'] = count
            list_run = []
            list_run.append({"portfolio":portfolio, "expected_profit": float(self.request.POST['income']))})
            step_2, step_5, step_6 = pool.map(markus_run, list_run)
            context['step_2'] = step_2
            context['step_5'] = step_5
            context['step_6'] = step_6
        context['graph'] = get_url('graph.png')
        return self.render_to_response(context)

class OptimalPortfolioView(TemplateView):
    template_name = 'optimal_portfolio.html'
